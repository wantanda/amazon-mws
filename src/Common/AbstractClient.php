<?php

namespace SellerWorks\Amazon\Common;

use GuzzleHttp\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Uri;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Psr\Http\Message\UriInterface;
use SellerWorks\Amazon\Common\Event\RequestEvent;
use SellerWorks\Amazon\Common\Exception\ErrorException;
use SellerWorks\Amazon\Credentials\CredentialsAwareInterface;
use SellerWorks\Amazon\Credentials\CredentialsAwareTrait;
use SellerWorks\Amazon\Credentials\CredentialsInterface;
use SellerWorks\Amazon\Events;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use UnexpectedValueException;
use function GuzzleHttp\Psr7\build_query;

/**
 * Base client class for all MWS endpoints.
 */
class AbstractClient implements ClientInterface, CredentialsAwareInterface
{

    /**
     * @property $credentials
     * @method   CredentialsInterface  getCredentials()
     * @method   self  setCredentials(CredentialsInterface $credentials)
     */
    use CredentialsAwareTrait;

    /**
     * MWS service definitions.
     */
    const MWS_PATH    = '';
    const MWS_VERSION = '';

    /** @var Client */
    protected $guzzle;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var UriInterface */
    protected $uri;

    /** @var string */
    protected $defaultMarketplaceId;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /**
     * List of Amazon MWS URLs, indexed by country code.
     *
     * @var array
     */
    protected $countryInfo = [
        // NA region
        Country::CA => ['host' => 'mws.amazonservices.ca', 'marketplaceId' => 'A2EUQ1WTGCTBG2'],
        Country::MX => ['host' => 'mws.amazonservices.com.mx', 'marketplaceId' => 'A1AM78C64UM0Y8'],
        Country::US => ['host' => 'mws.amazonservices.com', 'marketplaceId' => 'ATVPDKIKX0DER'],
        // EU region
        Country::DE => ['host' => 'mws-eu.amazonservices.com', 'marketplaceId' => 'A1PA6795UKMFR9'],
        Country::ES => ['host' => 'mws-eu.amazonservices.com', 'marketplaceId' => 'A1RKKUPIHCS9HS'],
        Country::FR => ['host' => 'mws-eu.amazonservices.com', 'marketplaceId' => 'A13V1IB3VIYZZH'],
        Country::IN => ['host' => 'mws.amazonservices.in', 'marketplaceId' => 'A21TJRUUN4KGV'],
        Country::IT => ['host' => 'mws-eu.amazonservices.com', 'marketplaceId' => 'APJ6JRA9NG5V4'],
        Country::UK => ['host' => 'mws-eu.amazonservices.com', 'marketplaceId' => 'A1F83G8C2ARO7P'],
        // FE region
        Country::JP => ['host' => 'mws.amazonservices.jp', 'marketplaceId' => 'A1VC38T7YXB528'],
        // CN region
        Country::CN => ['host' => 'mws.amazonservices.com.cn', 'marketplaceId' => 'AAHKV2X7AFYLW'],
    ];

    /**
     * AbstractClient constructor.
     *
     * @param CredentialsInterface $credentials
     * @param string               $countryCode Defined constant from Country
     */
    public function __construct(CredentialsInterface $credentials, string $countryCode = Country::US)
    {
        $this->guzzle = new GuzzleClient;
        $this->serializer = new Serializer\Serializer;
        $this->setCredentials($credentials);
        $this->setCountry($countryCode);
    }

    /**
     * @param string $countryCode
     *
     * @return AbstractClient
     */
    protected function setCountry(string $countryCode): self
    {
        if (!array_key_exists($countryCode, $this->countryInfo)) {
            throw new InvalidArgumentException(sprintf('Unknown country code: "%s"', $countryCode));
        }
        $this->uri = $this->buildUri($this->countryInfo[$countryCode]['host']);
        $this->defaultMarketplaceId = $this->countryInfo[$countryCode]['marketplaceId'];

        return $this;
    }

    /**
     * @param string $host
     *
     * @return UriInterface
     */
    private function buildUri(string $host): UriInterface
    {
        return new Uri(sprintf('https://%s/%s', $host, trim(static::MWS_PATH, '/')));
    }

    protected function send(RequestInterface $request, int $throttle = 30): PromiseInterface
    {
        $requestEvent = new RequestEvent($request);
        $this->getEventDispatcher()->dispatch(Events::REQUEST, $requestEvent);
        $client = $this;
        $headers = ['Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8', 'Expect' => ''];
        $query = $this->buildQuery($request);
        $gzRequest = new GuzzleRequest('POST', $this->uri, $headers, $query);
        $promise = $this->guzzle->sendAsync($gzRequest)->then(
            function (PsrResponseInterface $response) use ($client) {
                $contents = $this->serializer->unserialize($response->getBody()->getContents());
                if (!$contents instanceof ResponseInterface) {
                    return $contents;
                }
                $result = $contents->getResult();
                if ($result instanceof IterableResultInterface) {
                    $result->setClient($this);
                }

                return $result;
            },
            function (RequestException $e) use ($request, $throttle) {
                $contents = $e->getResponse()->getBody()->getContents();

                if (false === preg_match_all('#<(Type|Code|Message)>(.*?)</#si', $contents, $matches)) {
                    throw new ErrorException($e->getMessage());
                }
                $error = array_combine($matches[1], $matches[2]);
                if ($error['Code'] == 'RequestThrottled') {
                    sleep($throttle);

                    return $this->send($request, $throttle);
                }
                throw new ErrorException($error['Message']);
            }
        );

        return $promise;
    }

    protected function getEventDispatcher(): EventDispatcherInterface
    {
        if (!$this->eventDispatcher instanceof EventDispatcherInterface) {
            $this->eventDispatcher = new EventDispatcher();
        }

        return $this->eventDispatcher;
    }

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): self
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }

    private function buildQuery(RequestInterface $request): string
    {
        if (!$this->credentials instanceof CredentialsInterface) {
            throw new UnexpectedValueException('Set credentials to use this service.');
        }
        $credentials = $this->credentials;
        $parameters = $this->serializer->serialize($request);
        $parameters['SellerId'] = $credentials->getSellerId();
        $parameters['AWSAccessKeyId'] = $credentials->getAccessKey();
        $parameters['MWSAuthToken'] = $credentials->getMwsAuthToken();
        $parameters['SignatureMethod'] = 'HmacSHA256';
        $parameters['SignatureVersion'] = 2;
        $parameters['Timestamp'] = $this->gmdate();
        $parameters['Version'] = static::MWS_VERSION;
        $parameters['MarketplaceId.Id.1'] = $this->defaultMarketplaceId;
        uksort($parameters, 'strcmp');
        $parameters['Signature'] = new Signature($this->uri, $parameters, $credentials->getSecretKey());

        return build_query($parameters);
    }

    private function gmdate(): string
    {
        return gmdate(SerializerInterface::DATE_FORMAT);
    }
}
