<?php

namespace SellerWorks\Amazon\Orders\Serializer;

use Sabre\Xml\Service;
use SellerWorks\Amazon\Orders\Request\GetOrderRequest;
use SellerWorks\Amazon\Orders\Request\GetServiceStatusRequest;
use SellerWorks\Amazon\Orders\Request\ListOrderItemsByNextTokenRequest;
use SellerWorks\Amazon\Orders\Request\ListOrderItemsRequest;
use SellerWorks\Amazon\Orders\Request\ListOrdersByNextTokenRequest;
use SellerWorks\Amazon\Orders\Request\ListOrdersRequest;
use UnexpectedValueException;

use SellerWorks\Amazon\Common\RequestInterface;
use SellerWorks\Amazon\Common\SerializerInterface;
use SellerWorks\Amazon\Common\Serializer\Serializer as BaseSerializer;
use SellerWorks\Amazon\Orders\Request;

/**
 * Request Serializer / Response Deserializer.
 */
final class Serializer extends BaseSerializer implements SerializerInterface
{
    /** @var XmlDeserializer */
    private $xmlDeserializer;

    /** Constructor */
    public function __construct()
    {
        $this->xmlDeserializer = new XmlDeserializer;
    }

    /**
     * {@inheritDoc}
     */
    public function serialize(RequestInterface $request)
    {
        // Validate request is valid type and set action.
        switch (true) {
            case $request instanceof ListOrdersRequest:
                $action = 'ListOrders';
                break;

            case $request instanceof ListOrdersByNextTokenRequest:
                $action = 'ListOrdersByNextToken';
                break;

            case $request instanceof GetOrderRequest:
                $action = 'GetOrder';
                break;

            case $request instanceof ListOrderItemsRequest:
                $action = 'ListOrderItems';
                break;

            case $request instanceof ListOrderItemsByNextTokenRequest:
                $action = 'ListOrderItemsByNextToken';
                break;

            case $request instanceof GetServiceStatusRequest:
                $action = 'GetServiceStatus';
                break;

            default:
                throw new UnexpectedValueException(get_class($request) . ' is not supported.');
        }

        return $this->serializeProperties($action, $request);
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($response)
    {
        return $this->xmlDeserializer->parse($response);
    }
}
