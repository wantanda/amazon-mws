<?php
namespace SellerWorks\Amazon\Orders;

use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use SellerWorks\Amazon\Common\ResultInterface;
use SellerWorks\Amazon\Orders\Request\GetOrderRequest;
use SellerWorks\Amazon\Orders\Request\GetServiceStatusRequest;
use SellerWorks\Amazon\Orders\Request\ListOrderItemsByNextTokenRequest;
use SellerWorks\Amazon\Orders\Request\ListOrderItemsRequest;
use SellerWorks\Amazon\Orders\Request\ListOrdersByNextTokenRequest;
use SellerWorks\Amazon\Orders\Request\ListOrdersRequest;
use SellerWorks\Amazon\Orders\Result\GetOrderResult;
use SellerWorks\Amazon\Orders\Result\GetServiceStatusResult;
use SellerWorks\Amazon\Orders\Result\ListOrderItemsResult;
use SellerWorks\Amazon\Orders\Result\ListOrdersResult;

trait OrdersTrait
{

    public function ListOrders(ListOrdersRequest $request): ListOrdersResult
    {
        return $this->ListOrdersAsync($request)->wait();
    }

    public function ListOrdersAsync(ListOrdersRequest $request, int $throttle = 10): PromiseInterface
    {
        return $this->send($request, $throttle);
    }

    public function ListOrdersByNextToken(ListOrdersByNextTokenRequest $request): ListOrdersResult
    {
        return $this->ListOrdersByNextTokenAsync($request)->wait();
    }

    public function ListOrdersByNextTokenAsync(ListOrdersByNextTokenRequest $request, int $throttle = 10): PromiseInterface
    {
        return $this->send($request, $throttle);
    }

    public function GetOrder(GetOrderRequest $request): GetOrderResult
    {
        return $this->GetOrderAsync($request)->wait();
    }

    public function GetOrderAsync(GetOrderRequest $request, int $throttle = 3): PromiseInterface
    {
        return $this->send($request, $throttle);
    }

    public function ListOrderItems(ListOrderItemsRequest $request): ListOrderItemsResult
    {
        return $this->ListOrderItemsAsync($request)->wait();
    }

    public function ListOrderItemsAsync(ListOrderItemsRequest $request, int $throttle = 10): PromiseInterface
    {
        return $this->send($request, $throttle);
    }

    public function ListOrderItemsByNextToken(ListOrderItemsByNextTokenRequest $request): ListOrderItemsResult
    {
        return $this->ListOrderItemsByNextTokenAsync($request)->wait();
    }

    public function ListOrderItemsByNextTokenAsync(ListOrderItemsByNextTokenRequest $request, int $throttle = 10): PromiseInterface
    {
        return $this->send($request, $throttle);
    }

    public function GetServiceStatus(): ResultInterface
    {
        return $this->GetServiceStatusAsync()->wait();
    }

    public function GetServiceStatusAsync(): PromiseInterface
    {
        return $this->send(new GetServiceStatusRequest());
    }
}
