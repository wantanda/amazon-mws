<?php
namespace SellerWorks\Amazon\Credentials;
/**
 * Credentials for all requests.
 */
final class Credentials implements CredentialsInterface
{

    /** @var string */
    protected $SellerId;

    /** @var string */
    protected $AccessKey;

    /** @var string */
    protected $SecretKey;

    /** @var string */
    protected $MwsAuthToken;

    /**
     * @param  string $SellerId
     * @param  string $AccessKey
     * @param  string $SecretKey
     * @param  string $MwsAuthToken
     */
    public function __construct(string $SellerId, string $AccessKey, string $SecretKey, string $MwsAuthToken = '')
    {
        $this->SellerId = trim($SellerId);
        $this->AccessKey = trim($AccessKey);
        $this->SecretKey = trim($SecretKey);
        $this->MwsAuthToken = trim($MwsAuthToken);
    }

    public function getSellerId(): string
    {
        return $this->SellerId;
    }

    public function getAccessKey(): string
    {
        return $this->AccessKey;
    }

    public function getSecretKey(): string
    {
        return $this->SecretKey;
    }

    public function getMwsAuthToken(): string
    {
        return $this->MwsAuthToken;
    }
}
